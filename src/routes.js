export default {
  '/': 'Home',
  '/about': 'About',
  '/word-directeur': 'WordDirecteur',
  '/college-presentation': 'PresentationCollege',
  '/college-historique': 'CollegeHistorique',
  '/calendrier-scolaire': 'CalendrierScolaire',
  '/fondation': 'Fondation',
  '/notre-equipe': 'Equipe',
  '/passage-primaire-secondaire': 'PassagePrimaireSecondaire'
}
